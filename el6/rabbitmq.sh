#!/bin/bash

ERLANG_MAJOR="21"
ERLANG_VERSION="21.1.2-1"
RABBITMQ_VERSION="3.7.9"

genpasswd() { 
    local l=$1
    [ "$l" == "" ] && l=16
    tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs 
}

command_exists() {
	command -v "$@" > /dev/null 2>&1
}

prerequisites() {
    if command_exists socat logrotate; then
        return 0
    else
        echo "Installing dependencies ..."
        yum install -y epel-release
        yum install -y socat logrotate
    fi
}

install()
{
    TEMP_DIR=`mktemp -d`
    cd "$TEMP_DIR"

    yum install -y https://dl.bintray.com/rabbitmq/rpm/erlang/$ERLANG_MAJOR/el/6/x86_64/erlang-$ERLANG_VERSION.el6.x86_64.rpm
    rpm --import https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc
    yum install -y https://dl.bintray.com/rabbitmq/all/rabbitmq-server/$RABBITMQ_VERSION/rabbitmq-server-$RABBITMQ_VERSION-1.el6.noarch.rpm
}

start_service()
{
    chkconfig rabbitmq-server on
    service rabbitmq-server start
}

create_user()
{
    RABBITMQ_PASSWD=`genpasswd`
    rabbitmqctl add_user rabbitmq $RABBITMQ_PASSWD
    rabbitmqctl set_user_tags rabbitmq administrator
    rabbitmqctl set_permissions -p / rabbitmq ".*" ".*" ".*"
}

main()
{
    if command_exists rabbitmqctl; then
        echo "Rabbitmq already found in your PATH."
        exit 0
    fi

    prerequisites
    install
    start_service
    create_user
    # Enable management plugin
    rabbitmq-plugins enable rabbitmq_management

    echo "*************************************************"
    echo "* Management Home: http://$(hostname -i):15672"
    echo "* The credential of \"rabbitmq\" is $RABBITMQ_PASSWD"
    echo "*************************************************"
}

main