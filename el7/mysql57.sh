#!/bin/bash

main()
{
    TEMP_DIR=`mktemp -d`
    MYSQL_RPM_NAME="${1:-mysql80-community-release-el7-1.noarch.rpm}"
    #
    # Get other rpms from the link below:
    #   https://repo.mysql.com/
    # 

    # Install yum-utils if it does not exist
    if rpm -qa | grep yum-utils > /dev/null; then
        echo "yum-config-manager exists."
    else
        yum install -y yum-utils
    fi

    cd "$TEMP_DIR"

    curl -O https://repo.mysql.com//$MYSQL_RPM_NAME
    rpm -Uvh $MYSQL_RPM_NAME
    yum-config-manager --disable mysql80-community
    yum-config-manager --enable mysql57-community
    yum repolist enabled | grep mysql
    yum install -y mysql-community-server
    # edit /etc/my.cnf
    cp /etc/my.cnf /etc/my.cnf.bak
    cat > /etc/my.cnf << EOF
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

net_write_timeout=60
lower_case_table_names=1
character_set_server=utf8
max_connections=1000
transaction-isolation=READ-COMMITTED

back_log = 300
max_connect_errors = 100
innodb_buffer_pool_size = 1G

log_output=FILE
general_log=ON
slow_query_log=ON

[client]

[mysql]
auto-rehash
EOF

    echo "Starting mysql service ..."
    systemctl start mysqld
    grep 'temporary password' /var/log/mysqld.log
}

main