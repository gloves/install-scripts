#!/bin/bash

readonly ERLANG_VERSION="23.2.7"
readonly RABBITMQ_VERSION="3.8.14"
eval $(cat /etc/os-release | grep VERSION_ID) # Get Enterprise Linux major version

genpasswd() {
  local l=$1
  [ "$l" == "" ] && l=16
  tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}

command_exists() {
	command -v "$@" > /dev/null 2>&1
}

prerequisites() {
  if command_exists socat logrotate; then
    return 0
  else
    echo "Installing dependencies ..."
    yum install -y epel-release
    yum install -y socat logrotate
  fi
}

install_erlang() {
  yum install -y \
  https://github.com/rabbitmq/erlang-rpm/releases/download/v$ERLANG_VERSION/erlang-$ERLANG_VERSION-1.el${VERSION_ID}.x86_64.rpm
}

install_rabbitmq() {
  rpm --import \
  https://github.com/rabbitmq/rabbitmq-server/releases/download/v$RABBITMQ_VERSION/rabbitmq-server-$RABBITMQ_VERSION-1.el${VERSION_ID}.noarch.rpm.asc
  yum install -y \
  https://github.com/rabbitmq/rabbitmq-server/releases/download/v$RABBITMQ_VERSION/rabbitmq-server-$RABBITMQ_VERSION-1.el${VERSION_ID}.noarch.rpm
}

perform_install()
{
  TEMP_DIR=`mktemp -d`
  cd "$TEMP_DIR"
  install_erlang && install_rabbitmq
}

start_service()
{
  systemctl enable rabbitmq-server \
  && systemctl start rabbitmq-server
}

create_user()
{
  RABBITMQ_PASSWD=`genpasswd`
  rabbitmqctl add_user rabbitmq $RABBITMQ_PASSWD
  rabbitmqctl set_user_tags rabbitmq administrator
  rabbitmqctl set_permissions -p / rabbitmq ".*" ".*" ".*"
}

print_result() {
  echo "*************************************************"
  echo "* Management Home: http://$(hostname -i):15672"
  echo "* The credential of \"rabbitmq\" is $RABBITMQ_PASSWD"
  echo "*************************************************"
}

main()
{
  if command_exists rabbitmqctl; then
    echo "Rabbitmq already found in your \$PATH."
    exit 0
  fi

  prerequisites \
  && perform_install \
  && start_service \
  && create_user \
  && rabbitmq-plugins enable rabbitmq_management \
  && print_result
}

main $@
