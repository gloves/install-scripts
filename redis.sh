#!/bin/bash

genpasswd() { 
	local l=$1
    [ "$l" == "" ] && l=16
    tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs 
}

main()
{
    if rpm -aq | grep epel-release > /dev/null; then
        echo "epel-release already installed."
    else
        eval $(cat /etc/os-release | grep VERSION_ID)
        yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-${VERSION_ID}.noarch.rpm
    fi

    yum install -y redis

    REDIS_PASSWD=`genpasswd`
    sed -i 's/# requirepass foobared/requirepass '$REDIS_PASSWD'/g' /etc/redis.conf
    sed -i 's/^bind 127\.0\.0\.1/bind 0.0.0.0/g' /etc/redis.conf

    # Start and enable redis service
    systemctl start redis.service
    systemctl enable redis.service

    echo "*******************************************"
    echo "* Your redis password is $REDIS_PASSWD"
    echo "*******************************************"
}

main $@