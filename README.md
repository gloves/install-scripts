# install-scripts

提供 HAP 框架依赖的、并非由 haps 自动托管的组件的一键安装脚本。

- MySQL 5.7

```bash
# Enterprise Linux 7
curl -s https://gitlab.com/gloves/install-scripts/raw/master/el7/mysql57.sh | sudo bash
```

- RabbitMQ 3.7.5

```bash
# Enterprise Linux 6
curl -s https://gitlab.com/gloves/install-scripts/raw/master/el6/rabbitmq.sh | sudo bash
# Enterprise Linux 7 and 8
curl -s https://gitlab.com/gloves/install-scripts/raw/master/rabbitmq.sh | sudo bash
```

- Redis (RPM 方式)

```bash
curl -s https://gitlab.com/gloves/install-scripts/raw/master/redis.sh | sudo bash
```

> 提示：Glove 工具已托管基于源码编译安装的 Redis
>

## 参考链接

----

- http://www.rabbitmq.com/install-rpm.html
- http://www.rabbitmq.com/management.html
- https://www.rabbitmq.com/rabbitmqctl.8.html
- https://dev.mysql.com/doc/mysql-yum-repo-quick-guide/en/
- https://fedoraproject.org/wiki/EPEL
- https://www.digitalocean.com/community/tutorials/how-to-install-secure-redis-centos-7
- https://www.cyberciti.biz/faq/linux-random-password-generator/
- https://blog.sleeplessbeastie.eu/2012/07/07/centos-how-to-manage-system-services/


